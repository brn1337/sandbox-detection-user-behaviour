import logging
import math
import operator
import sys
import time
import win32api

from PIL import ImageGrab

# In order to create the exe:
# pyinstaller --onefile --windowed --clean --name=concept.exe --key=SOMERANDOMSTRING concept.py


RMS_TOLERANCE = 10000
TOT_CLICKS = 10
RIGHT_CLICK = False
LEFT_CLICK = True
SLEEP_INT = 0.01

LOGGING_LEVEL = logging.INFO
LOGGING_FILENAME = None  # "log.txt"
logging.basicConfig(stream=sys.stderr, level=LOGGING_LEVEL, filename=LOGGING_FILENAME)

if sys.version_info[0] >= 3:
    xrange = range


def wait_click(num_clicks):
    # Thanks to https://stackoverflow.com/questions/165495/detecting-mouse-clicks-in-windows-using-python
    click_counter = 0
    state_left = win32api.GetKeyState(0x01)
    state_right = win32api.GetKeyState(0x02)

    while click_counter < num_clicks:
        a = win32api.GetKeyState(0x01)
        b = win32api.GetKeyState(0x02)

        if LEFT_CLICK and a != state_left:
            state_left = a
            if a < 0:
                click_counter += 1
                logging.debug('Left Button Pressed')
            else:
                logging.debug('Left Button Released')

        if RIGHT_CLICK and b != state_right:
            state_right = b
            if b < 0:
                click_counter += 1
                logging.debug('Right Button Pressed')
            else:
                logging.debug('Right Button Released')

        time.sleep(SLEEP_INT)    


def get_rms():
    # Thanks to https://stackoverflow.com/questions/1927660/compare-two-images-the-python-linux-way
    rms_list = []
    for x in xrange(0, TOT_CLICKS):
        logging.debug('Getting screenshot %s part 1/2' % x)
        h1 = ImageGrab.grab().histogram()
        wait_click(1)
        logging.debug('Getting screenshot %s part 2/2' % x)
        h2 = ImageGrab.grab().histogram()
        rms = math.sqrt(reduce(operator.add, map(lambda a,b: (a-b)**2, h1, h2))/len(h1))
        rms_list.append(rms)
    return rms_list


def compare_rms(rms_difference):
    logging.debug("Comparing RMS of the list: %s" % rms_difference)
    rms_average = reduce(lambda x, y: x + y, rms_difference) / len(rms_difference)
    logging.debug("The RMS average is: %s" % rms_average)
    if rms_average > RMS_TOLERANCE:
        logging.info('RMS test passed')
        return True
    else:
        logging.info('RMS test failed')
        return False


def good_code():
    logging.info("Good code executed")


def evil_code():
    logging.info("Evil code executed")



def main():
    evaluation = compare_rms(get_rms())
    if evaluation == True:
        evil_code()
    else:
        good_code()


if __name__ == '__main__':
    main()