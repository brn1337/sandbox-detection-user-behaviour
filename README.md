# Sandbox awareness via user behaviour
A technique to evade Sandboxes by identifying real user behaviour in a short and reliable way.

# Context
**Sandbox evasion techniques** are divided in 3 main categories:
- Detection
- Exploitation
- **Context-Awareness** (Environment-Sensitiveness)

I focused on "Context-Awareness" because it is indipendent from the virtualization technology and the technology version.

# Features
- No loggable behaviour during sanbox detection
- Can detect real user in less than a minute
- Technology indipendent (VMWare, Hyper-V, ...)
- Version indipendent (CVE-2014-0983, CVE-2015-8986, ...)
- Sandox Indipendent (JoeBox, CWSandBox, ...)
- Hard to mitigate

# External Modules
- PIL

# How it works
The assumption underlying the success of this technique is that Sanbox won't run other softwares to avoid overlapping behaviours, but real users do!

The code takes a screenshot each time the user click, then  using the root-mean-square difference compares the images, if the images are actually different the click was a meaningful action. If a minimum tolerance between multiple comparisions is exceeded the evil code is executed.

# Conclusions
From a vendor point of view it is expensive to deceive this technique because it requires to:
- add mouse interaction
- run extra software
- differentiate the software and the malware activities

And also cause the inability to use consistently screenshots during the analysis.
